<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CVDA</title>
</head>
<body>


<?php

use demo\Account;

require_once("Account.php");

$acc1 = new Account("Théo", "LeRoux", "mail@gogo.com", "pass123");
$acc2 = new Account("Théo", "LeBOnd", "mail@gogo.com", "pass123");


echo $acc1;
echo "<br>";
echo $acc2;
?>

</body>
</html>
